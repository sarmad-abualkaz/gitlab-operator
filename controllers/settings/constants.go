package settings

const (
	// Region .
	Region = "us-east-1"

	// RegistryBucket .
	RegistryBucket = "registry"

	// AppConfigConnectionSecretName .
	AppConfigConnectionSecretName = "storage-config"

	// RegistryConnectionSecretName .
	RegistryConnectionSecretName = "registry-storage"

	// TaskRunnerConnectionSecretName .
	TaskRunnerConnectionSecretName = "s3cmd-config"
)
